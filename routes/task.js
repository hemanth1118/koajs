const Router = require('koa-router')
const router = new Router()
const User = require('../models/user')

router.get('/api/user', async ctx => {
    await User.find()
    .then(user => {
        ctx.body = user
    }).catch(err => {
        ctx.body = err
    })
})

router.post('/', async ctx =>{
    if(!ctx.request.body.name){
        ctx.body = {
            error: 'Bad Data'
        }
    }else{
        var user = new User()
        user.name =ctx.request.body.name
        await user.save()
        .then(data =>{
            ctx.body = data
        }).catch(err => {
            ctx.body = err
            
        })
    }
})

router.delete('/api/user/:id', async ctx => {
   await User.deleteOne({
        _id: ctx.params.id
    }).then(( )=> {
         ctx.body = {status: "successfully deleted"}
    }).catch(err => {
         ctx.body = 'error' + err
    })
})

router.get('/api/user/:id', async ctx => {
    await User.findById({
        _id: ctx.params.id
    }).then(data => {
        ctx.body = data
    }).catch(err => {
        ctx.body = err
    })
})
router.put('/api/user/:id', async ctx => {
    await User.findOneAndUpdate(
        {_id: ctx.params.id},{name: ctx.request.body.name}).then(data => {
          ctx.body = data
     }).catch(err => {
          ctx.body = 'error' + err
     })
 })

module.exports = router