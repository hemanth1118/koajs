const Koa = require('koa')
const KoaRouter = require('koa-router')
const json = require('koa-json')
const path = require('path')
const render = require('koa-ejs')
const app = new Koa()
const router = new KoaRouter();
const bodyparser = require('koa-bodyparser')
const mongoose = require('mongoose')

const user = require('./routes/task')

app.use(bodyparser());
app.use(user.routes());

mongoose.connect(
    'mongodb://localhost/koajstasks',
    { useNewUrlParser: true }
)


// // Replace with Db
// const things = ['Friends', 'Programming', 'Music', 'Workout']

// // Json Prettier Middleware
// app.use(json())

// //BodyParser Middleware 

// // app.use(async ctx => {ctx.body = {msg: 'hello world'}})

// render(app, {
//     root: path.join(__dirname, 'views'),
//     layout: 'layout',
//     viewExt: 'html',
//     cache: false,
//     debug: false
// })

// // Routes
// router.get('/',index);
// router.get('/add',showAdd);
// router.post('/add',add);



// // List of functions 
// async function index(ctx){
//     await ctx.render('index',{
//         things: things
//     })
// }

// // ShowAdd Page
// async function showAdd(ctx){
//     await ctx.render('add')
// }

// // add thing 
// async function add(ctx){
//     const body = ctx.request.body
//     things.push(body.thing)
//     ctx.redirect('/')
// }

// router.get('/test', ctx => (ctx.body = 'Hello Test'));

// Router Middleware
// app.use(router.routes()).use(router.allowedMethods());

app.listen(3000, () => { console.log("server is started on port 3000.....")})